package com.ufro.TareaDocker.models;

public class Usuario {
    private String nombre;
    private String rut;
    private String usuario;
    private String contrasena;

    public Usuario() {
    }

    public Usuario(String nombre, String rut, String usuario, String contrasena) {
        this.nombre = nombre;
        this.rut = rut;
        this.usuario = usuario;
        this.contrasena = contrasena;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }
}
