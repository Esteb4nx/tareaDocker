package com.ufro.TareaDocker.models;

public class Merma {
    private String nombre;
    private MOTIVO motivo;
    private double cantidad;

    public Merma() {
    }

    public Merma(String nombre, MOTIVO motivo, double cantidad) {
        this.nombre = nombre;
        this.motivo = motivo;
        this.cantidad = cantidad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public MOTIVO getMotivo() {
        return motivo;
    }

    public void setMotivo(MOTIVO motivo) {
        this.motivo = motivo;
    }

    public double getCantidad() {
        return cantidad;
    }

    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;
    }
}
