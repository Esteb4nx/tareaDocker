package com.ufro.TareaDocker.models;

public class Reponedor extends Usuario{
    private SECCION seccion;

    public Reponedor() {
    }

    public Reponedor(String nombre, String rut, String usuario, String contrasena, SECCION seccion) {
        super(nombre, rut, usuario, contrasena);
        this.seccion = seccion;
    }

    public SECCION getSeccion() {
        return seccion;
    }

    public void setSeccion(SECCION seccion) {
        this.seccion = seccion;
    }
}
