package com.ufro.TareaDocker.models;

public class Producto {
    private String nombre;
    private SECCION seccion;
    private int stock;
    private double precio;
    private UBICACION ubicacion;

    public Producto() {
    }

    public Producto(String nombre, SECCION seccion, int stock, double precio, UBICACION ubicacion) {
        this.nombre = nombre;
        this.seccion = seccion;
        this.stock = stock;
        this.precio = precio;
        this.ubicacion = ubicacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public SECCION getSeccion() {
        return seccion;
    }

    public void setSeccion(SECCION seccion) {
        this.seccion = seccion;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public UBICACION getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(UBICACION ubicacion) {
        this.ubicacion = ubicacion;
    }
}
