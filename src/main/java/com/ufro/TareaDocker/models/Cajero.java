package com.ufro.TareaDocker.models;

public class Cajero extends Usuario{
    private int ventas;

    public Cajero() {
    }

    public Cajero(String nombre, String rut, String usuario, String contrasena, int ventas) {
        super(nombre, rut, usuario, contrasena);
        this.ventas = ventas;
    }

    public int getVentas() {
        return ventas;
    }

    public void setVentas(int ventas) {
        this.ventas = ventas;
    }
}
