package com.ufro.TareaDocker.models;

public enum SECCION {
    LACTEOS,
    PANADERIA,
    CARNICERIA,
    FRUTERIA,
    BOTILLERIA
}
