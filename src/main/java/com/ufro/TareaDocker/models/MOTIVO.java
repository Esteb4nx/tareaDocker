package com.ufro.TareaDocker.models;

public enum MOTIVO {
    ROBO_EMPLEADO,
    ROBO_CLIENTE,
    DESCONOCIDO
}
