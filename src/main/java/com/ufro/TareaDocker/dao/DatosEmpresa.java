package com.ufro.TareaDocker.dao;

import com.ufro.TareaDocker.models.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.function.Predicate;

public class DatosEmpresa {
    private ArrayList<Producto> productos;
    private ArrayList<Reponedor> reponedores;
    private ArrayList<Cajero> cajeros;
    private ArrayList<Merma> mermas;

    public DatosEmpresa() {
        // Instancia Productos

        Producto producto1 = new Producto("Leche", SECCION.LACTEOS,10,800,UBICACION.PASILLO_A);
        Producto producto2 = new Producto("Yogur", SECCION.LACTEOS,50,200,UBICACION.PASILLO_A);
        Producto producto3 = new Producto("Cerveza", SECCION.BOTILLERIA,6,1000,UBICACION.PASILLO_B);
        Producto producto4 = new Producto("Vino", SECCION.BOTILLERIA,4,3400,UBICACION.PASILLO_B);
        Producto producto5 = new Producto("Marraqueta", SECCION.PANADERIA,20,1800,UBICACION.PASILLO_C);
        Producto producto6 = new Producto("Pan de molde", SECCION.PANADERIA,20,2000,UBICACION.PASILLO_C);
        Producto producto7 = new Producto("Frutillas", SECCION.FRUTERIA,500,2000,UBICACION.PASILLO_D);
        Producto producto8 = new Producto("Lomo liso", SECCION.CARNICERIA,12,18000,UBICACION.PASILLO_E);

        productos = new ArrayList<>(Arrays.asList(producto1, producto2, producto3, producto4, producto5, producto6, producto7, producto8));

        // Instancia Reponedores
        Reponedor reponedor1 = new Reponedor("Jorge","11111111-1","Koke","123",SECCION.LACTEOS);
        Reponedor reponedor2 = new Reponedor("Isidora","22222222-2","Ichi","123",SECCION.PANADERIA);
        Reponedor reponedor3 = new Reponedor("Fran","33333333-3","Franmander","123",SECCION.BOTILLERIA);

        reponedores = new ArrayList<>(Arrays.asList(reponedor1,reponedor2,reponedor3));
        // Instancia Cajeros
        Cajero cajero1 = new Cajero("Esteban","11111111-1","Esteb4n","123",20);
        Cajero cajero2 = new Cajero("Carlos","11111111-1","Carlos123","123",10);
        Cajero cajero3 = new Cajero("Juan","11111111-1","Juan123","123",30);
        Cajero cajero4 = new Cajero("Roberto","11111111-1","Rober123","123",15);
        Cajero cajero5 = new Cajero("Andres","11111111-1","Andy123","123",12);

        cajeros = new ArrayList<>(Arrays.asList(cajero1,cajero2,cajero3,cajero4,cajero5));

        // Instancia Mermas
        Merma merma1 = new Merma("Robo de Leche",MOTIVO.ROBO_CLIENTE,2);
        Merma merma2 = new Merma("Robo de Frutillas",MOTIVO.ROBO_EMPLEADO,100);
        Merma merma3 = new Merma("Falta stock Pan de molde",MOTIVO.DESCONOCIDO,5);

        mermas = new ArrayList<>(Arrays.asList(merma1,merma2,merma3));
    }

    public ArrayList<Producto> getProductos() {
        return productos;
    }

    public ArrayList<Reponedor> getReponedores() {
        return reponedores;
    }

    public ArrayList<Cajero> getCajeros() {
        return cajeros;
    }

    public ArrayList<Merma> getMermas() {
        return mermas;
    }


    // Refactorizable con un hashmap que para cada class type idntifique el arraylist al que acceder, de momento lo deje asi pq me dio flojera jaja
    public <T> T findByName(Class<T> tipo, String nombre){
        if(tipo == Producto.class){
            return (T) findByProperty(this.getProductos(), producto -> nombre.equals(producto.getNombre()));
        }

        if(tipo == Reponedor.class){
            return (T) findByProperty(this.getReponedores(), reponedor -> nombre.equals(reponedor.getNombre()));
        }

        if(tipo == Cajero.class){
            return (T) findByProperty(this.getCajeros(), cajero -> nombre.equals(cajero.getNombre()));
        }

        if(tipo == Merma.class){
            return (T) findByProperty(this.getMermas(), merma -> nombre.equals(merma.getNombre()));
        }

        return null;

    }

    public static <T> T findByProperty(Collection<T> lista, Predicate<T> filtro) {
        return lista.stream().filter(filtro).findFirst().orElse(null);
    }
}
