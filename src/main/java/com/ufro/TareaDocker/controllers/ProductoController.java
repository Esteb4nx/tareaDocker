package com.ufro.TareaDocker.controllers;

import com.ufro.TareaDocker.dao.DatosEmpresa;
import com.ufro.TareaDocker.models.Producto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@CrossOrigin
@RestController
@RequestMapping("producto")
public class ProductoController {

    public static DatosEmpresa datos = new DatosEmpresa();

    @GetMapping
    public ResponseEntity<ArrayList<Producto>> getAll(){
        return new ResponseEntity<>(datos.getProductos(),HttpStatus.OK);
    }

    @GetMapping("{nombre}")
    public ResponseEntity<Producto> getByName(@PathVariable String nombre){
        Producto producto = datos.findByName(Producto.class, nombre);
        if(producto == null){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(producto, HttpStatus.OK);
    }
}
