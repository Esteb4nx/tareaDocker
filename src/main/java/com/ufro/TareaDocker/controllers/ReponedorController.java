package com.ufro.TareaDocker.controllers;

import com.ufro.TareaDocker.dao.DatosEmpresa;
import com.ufro.TareaDocker.models.Reponedor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@CrossOrigin
@RestController
@RequestMapping("reponedor")
public class ReponedorController {

    public static DatosEmpresa datos = new DatosEmpresa();

    @GetMapping
    public ResponseEntity<ArrayList<Reponedor>> getAll(){
        return new ResponseEntity<>(datos.getReponedores(), HttpStatus.OK);
    }

    @GetMapping("{nombre}")
    public ResponseEntity<Reponedor> getByName(@PathVariable String nombre){
        Reponedor reponedor = datos.findByName(Reponedor.class, nombre);
        if(reponedor == null){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(reponedor, HttpStatus.OK);
    }

}
