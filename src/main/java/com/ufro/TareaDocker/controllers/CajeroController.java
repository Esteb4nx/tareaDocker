package com.ufro.TareaDocker.controllers;

import com.ufro.TareaDocker.dao.DatosEmpresa;
import com.ufro.TareaDocker.models.Cajero;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@CrossOrigin
@RestController
@RequestMapping("cajero")
public class CajeroController {

    public static DatosEmpresa datos = new DatosEmpresa();

    @GetMapping
    public ResponseEntity<ArrayList<Cajero>> getAll(){
        return new ResponseEntity<>(datos.getCajeros(), HttpStatus.OK);
    }

    @GetMapping("{nombre}")
    public ResponseEntity<Cajero> getByName(@PathVariable String nombre){
        Cajero cajero = datos.findByName(Cajero.class, nombre);
        if(cajero == null){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(cajero, HttpStatus.OK);
    }

}
