package com.ufro.TareaDocker.controllers;

import com.ufro.TareaDocker.dao.DatosEmpresa;
import com.ufro.TareaDocker.models.Merma;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@CrossOrigin
@RestController
@RequestMapping("merma")
public class MermaController {

    public static DatosEmpresa datos = new DatosEmpresa();

    @GetMapping
    public ResponseEntity<ArrayList<Merma>> getAll(){
        return new ResponseEntity<>(datos.getMermas(), HttpStatus.OK);
    }

    @GetMapping("{nombre}")
    public ResponseEntity<Merma> getByName(@PathVariable String nombre){
        Merma merma = datos.findByName(Merma.class, nombre);
        if(merma == null){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(merma, HttpStatus.OK);
    }
}
