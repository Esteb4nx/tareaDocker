package com.ufro.TareaDocker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TareaDockerApplication {

	public static void main(String[] args) {
		SpringApplication.run(TareaDockerApplication.class, args);
	}

}
